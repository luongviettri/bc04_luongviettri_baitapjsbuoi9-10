// lưu các hàm xử lý trên giao diện 
function layThongTinTuForm() {
    var taiKhoan = document.getElementById('tknv').value;
    var hoTen = document.getElementById('name').value;
    var email = document.getElementById('email').value;
    var matKhau = document.getElementById('password').value;
    var ngayLam = document.getElementById('datepicker').value;
    var luongCoBan = document.getElementById('luongCB').value;
    var chucVu = document.getElementById('chucvu').value;
    var gioLamTrongThang = document.getElementById('gioLam').value;
    return new NhanVien(taiKhoan, hoTen, email, matKhau, ngayLam, luongCoBan, chucVu, gioLamTrongThang);
}
function resetForm() {
    document.getElementById('tknv').value = '';
    document.getElementById('name').value = '';
    document.getElementById('email').value = '';
    document.getElementById('password').value = '';
    document.getElementById('datepicker').value = '';
    document.getElementById('luongCB').value = '';
    document.getElementById('chucvu').value = '';
    document.getElementById('gioLam').value = '';
}
function resetInput() {
    document.getElementById('tbTKNV').innerHTML = '';
    document.getElementById('tbTen').innerHTML = '';
    document.getElementById('tbEmail').innerHTML = '';
    document.getElementById('tbMatKhau').innerHTML = '';
    document.getElementById('tbNgay').innerHTML = '';
    document.getElementById('tbLuongCB').innerHTML = '';
    document.getElementById('tbChucVu').innerHTML = '';
    document.getElementById('tbGiolam').innerHTML = '';

}
function renderTable(nvArr) {
    var tbody = document.getElementById('tableDanhSach');
    var rows = '';
    for (var i = 0; i < nvArr.length; i++) {
        var row =
            `
        <tr>
        <td>${nvArr[i].taiKhoan} </td>
        <td>${nvArr[i].hoTen} </td>
        <td>${nvArr[i].email} </td>
        <td>${nvArr[i].ngayLam} </td>
        <td>${nvArr[i].chucVu} </td>
        <td>${nvArr[i].tongLuong()} </td>
        <td>${nvArr[i].xepLoai()} </td>
        <td> 
            <button data-toggle="modal" data-target="#myModal" 
            onclick='suaNhanVien("${nvArr[i].taiKhoan}")' 
            
            class="btn btn-warning mx-2 my-2">Sửa</button>
            <button
            onclick='xoaNhanVien("${nvArr[i].taiKhoan}")' 
          
            class="btn btn-danger ">Xóa</button>
        </td>
        </tr>
        `
        rows += row;

    }
    tbody.innerHTML = rows;
}
function dienThongTinLenForm(taikhoan, ten, mail, matKhau, ngayLam, luongCoBan, chucVu, gioLam) {
    document.getElementById('tknv').value = taikhoan;
    document.getElementById('name').value = ten;
    document.getElementById('email').value = mail;
    document.getElementById('password').value = matKhau;
    document.getElementById('datepicker').value = ngayLam;
    document.getElementById('luongCB').value = luongCoBan;
    document.getElementById('chucvu').value = chucVu;
    document.getElementById('gioLam').value = gioLam;
}
