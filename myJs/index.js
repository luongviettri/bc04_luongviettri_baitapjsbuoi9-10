// ! nhận thông tin từ màn hình
// ! tạo thành một instance mới
// ! lưu instance mới trong mảng đối tượng
var nvArr = [];
// ! tạo key local storage
const DANHSACHNHANVIEN = "DANHSACHNHANVIEN";
// ! lấy dữ liệu từ localStorage về 
var nvArrJSON = localStorage.getItem(DANHSACHNHANVIEN);
// ! render ra giao diện nếu như có dữ liệu
if (nvArrJSON) {
    nvArr = JSON.parse(nvArrJSON);
    // ! cập nhật hàm trong constructor
    for (var i = 0; i < nvArr.length; i++) {
        var nv = nvArr[i];
        nvArr[i] = new NhanVien(
            nv.taiKhoan,
            nv.hoTen,
            nv.email,
            nv.matKhau,
            nv.ngayLam,
            nv.luongCoBan,
            nv.chucVu,
            nv.gioLamTrongThang,
        )
    }
    renderTable(nvArr);
}

function handleAddButton() {
    var header = document.getElementById('header-title');
    header.innerText = "Log In";

    document.getElementById('tknv').disabled = false;
    document.getElementById('btnThemNV').disabled = false;
    // reset
    resetForm();
    resetInput();
}
function handleAccountValidation() {
    var newNv = layThongTinTuForm();
    // validate data
    var isValid = null;
    // ! tài khoản: kiểm tra rỗng và độ dài
    isValid =
        validator.kiemTraRong(newNv.taiKhoan, 'tbTKNV', "Vui lòng nhập tài khoản")
        && validator.kiemTraDoDai(newNv.taiKhoan, 'tbTKNV', "Độ dài hợp lệ từ 4 đến 6 kí tự", 4, 6)
        && validator.kiemTraTrungTaiKhoan(newNv.taiKhoan, 'tbTKNV', "Tài khoản bị trùng", nvArr);
}
function handleNameValidation() {
    var newNv = layThongTinTuForm();
    // validate data
    var isValid = null;
    // ! tên nhân viên: kiểm tra rỗng và kí tự số
    isValid =
        validator.kiemTraRong(newNv.hoTen, 'tbTen', "Vui lòng nhập họ tên")
        && validator.kiemTraKhongPhaiSo(newNv.hoTen, 'tbTen', "Tên ko dc chứa kí tự số")

}
function handleMailValidation() {
    var newNv = layThongTinTuForm();
    // validate data
    var isValid = null;
    // ! email nhân viên: kiểm tra rỗng và định dạng email
    isValid =
        validator.kiemTraRong(newNv.email, 'tbEmail', "Vui lòng nhập email")
        && validator.kiemTraEmail(newNv.email, 'tbEmail', "Email không hợp lệ")

}
function handlePassWordValidation() {
    var newNv = layThongTinTuForm();
    // validate data
    var isValid = null;
    // ! mật khẩu: kiểm tra rỗng, độ dài từ 6-10 kí tự chứa ít nhất 1 kí tự số, 1 kí tự in hoa, 1 kí tự đặc biệt
    isValid =
        validator.kiemTraRong(newNv.matKhau, 'tbMatKhau', "Vui lòng nhập mật khẩu")
        && validator.kiemTraMatKhau(newNv.matKhau, 'tbMatKhau', "Mật khẩu chứa từ 6-10 kí tự, chứa ít nhất 1 kí tự số, 1 kí tự in hoa, 1 kí tự đặc biệt")

}

function handleSalaryValidation() {
    var newNv = layThongTinTuForm();
    // validate data
    var isValid = null;
    // ! lương: kiếm tra rỗng và   lương từ 1tr đến 20tr
    isValid =
        validator.kiemTraRong(newNv.luongCoBan, 'tbLuongCB', "Vui lòng nhập lương")
        && validator.kiemTraMinMax(newNv.luongCoBan, 'tbLuongCB', "Lương từ 1 triệu đến 20 triệu", 1000000, 20000000);

}
function handleOfficeValidation() {
    var newNv = layThongTinTuForm();
    // validate data
    var isValid = null;
    // ! Chức vụ:
    isValid =
        validator.kiemTraChucVu(newNv.chucVu, 'tbChucVu', 'Vui lòng chọn chức vụ');
}
function handleWorkHourValidation() {
    var newNv = layThongTinTuForm();
    // validate data
    var isValid = null;
    // ! số giờ: rỗng và min max
    isValid = validator.kiemTraRong(newNv.gioLamTrongThang, 'tbGiolam', 'Vui lòng nhập giờ làm')
        && validator.kiemTraMinMax(newNv.gioLamTrongThang, 'tbGiolam', 'Giờ làm từ 80-200 giờ', 80, 200)
}

function themNhanVien() {
    var newNv = layThongTinTuForm();
    // validate data
    var isValid;
    // console.log('isValid: ', isValid);
    // ! tài khoản: kiểm tra rỗng và độ dài
    isValid =
        validator.kiemTraRong(newNv.taiKhoan, 'tbTKNV', "Vui lòng nhập tài khoản")
        && validator.kiemTraDoDai(newNv.taiKhoan, 'tbTKNV', "Độ dài hợp lệ từ 4 đến 6 kí tự", 4, 6)
        && validator.kiemTraTrungTaiKhoan(newNv.taiKhoan, 'tbTKNV', "Tài khoản bị trùng", nvArr);
    // ! tên nhân viên: kiểm tra rỗng và kí tự số
    isValid &=
        validator.kiemTraRong(newNv.hoTen, 'tbTen', "Vui lòng nhập họ tên")
        && validator.kiemTraKhongPhaiSo(newNv.hoTen, 'tbTen', "Tên ko dc chứa kí tự số")
    // ! email nhân viên: kiểm tra rỗng và định dạng email
    isValid &=
        validator.kiemTraRong(newNv.email, 'tbEmail', "Vui lòng nhập email")
        && validator.kiemTraEmail(newNv.email, 'tbEmail', "Email không hợp lệ")
    // ! mật khẩu: kiểm tra rỗng, độ dài từ 6-10 kí tự chứa ít nhất 1 kí tự số, 1 kí tự in hoa, 1 kí tự đặc biệt
    isValid &=
        validator.kiemTraRong(newNv.matKhau, 'tbMatKhau', "Vui lòng nhập mật khẩu")
        && validator.kiemTraMatKhau(newNv.matKhau, 'tbMatKhau', "Mật khẩu chứa từ 6-10 kí tự, chứa ít nhất 1 kí tự số, 1 kí tự in hoa, 1 kí tự đặc biệt")
    // ! ngày tháng năm: kiểm tra rỗng và định dạng ngày
    isValid &=
        validator.kiemTraRong(newNv.ngayLam, 'tbNgay', "Vui lòng nhập ngày")
        && validator.kiemTraNgay(newNv.ngayLam, 'tbNgay', "Vui lòng nhập đúng định dạng tháng/ ngày/ năm")
    // ! lương: kiếm tra rỗng và   lương từ 1tr đến 20tr
    isValid &=
        validator.kiemTraRong(newNv.luongCoBan, 'tbLuongCB', "Vui lòng nhập lương")
        && validator.kiemTraMinMax(newNv.luongCoBan, 'tbLuongCB', "Lương từ 1 triệu đến 20 triệu", 1000000, 20000000);
    // ! Chức vụ:
    isValid &=
        validator.kiemTraChucVu(newNv.chucVu, 'tbChucVu', 'Vui lòng chọn chức vụ');
    // ! số giờ: rỗng và min max
    isValid &= validator.kiemTraRong(newNv.gioLamTrongThang, 'tbGiolam', 'Vui lòng nhập giờ làm')
        && validator.kiemTraMinMax(newNv.gioLamTrongThang, 'tbGiolam', 'Giờ làm từ 80-200 giờ', 80, 200)
    if (!isValid) {
        return;
    }
    nvArr.push(newNv);
    //  lưu vào local storage
    var nvArrJSON = JSON.stringify(nvArr);
    localStorage.setItem(DANHSACHNHANVIEN, nvArrJSON);
    renderTable(nvArr);

    // ẩn model sau khi xử lý thành công:
    $('#myModal').modal('hide');
    $('body').removeClass('modal-open');
    $('body').css('padding-right', '0px');
    $('.modal-backdrop').remove();
    // reset form
    resetForm();
}
function xoaNhanVien(taiKhoanCanXoa) {
    // ! ES5:
    for (var i = 0; i < nvArr.length; i++) {
        if (nvArr[i].taiKhoan == taiKhoanCanXoa) {
            nvArr.splice(i, 1);
        }
    }
    // ! ES6:
    // const newArr = nvArr.filter((nv) => {
    //     if (nv.taiKhoan != taiKhoanCanXoa) {
    //         return nv;
    //     }
    // })
    // nvArr = [...newArr];
    // ! cập nhật lại arr trên json
    var nvArrJSON = JSON.stringify(nvArr);
    localStorage.setItem(DANHSACHNHANVIEN, nvArrJSON);
    // ! render ra table
    renderTable(nvArr);
}

function suaNhanVien(taiKhoanCanSua) {
    resetInput();
    // hiện ra header cần sửa
    var header = document.getElementById('header-title');
    header.innerText = "Sửa nhân viên";
    // ẩn trường tài khoản
    document.getElementById('tknv').disabled = true;
    document.getElementById('btnThemNV').disabled = true;
    // lấy thông tin của object điền vào màn hình
    //// todo: lấy thông tin của object
    var nvEdit = null;
    nvArr.forEach((nv) => {
        if (nv.taiKhoan == taiKhoanCanSua) {
            nvEdit = nv;
        }
    })
    //// todo: điền vào màn hình
    dienThongTinLenForm(nvEdit.taiKhoan, nvEdit.hoTen, nvEdit.email, nvEdit.matKhau, nvEdit.ngayLam, nvEdit.luongCoBan, nvEdit.chucVu, nvEdit.gioLamTrongThang);

}
function handleUpdate() {
    // lấy thông tin từ form và tạo thành object mới
    var newNv = layThongTinTuForm();
    var header = document.getElementById('header-title');
    if (header.innerText == "Log In") {
        alert("Không có gì để cập nhật");
        // ẩn model sau khi xử lý thành công:
        $('#myModal').modal('hide');
        $('body').removeClass('modal-open');
        $('body').css('padding-right', '0px');
        $('.modal-backdrop').remove();
        return;
    }
    // validate data
    var isValid = null;
    // ! tên nhân viên: kiểm tra rỗng và kí tự số
    isValid =
        validator.kiemTraRong(newNv.hoTen, 'tbTen', "Vui lòng nhập họ tên")
        && validator.kiemTraKhongPhaiSo(newNv.hoTen, 'tbTen', "Tên ko dc chứa kí tự số")
    // ! email nhân viên: kiểm tra rỗng và định dạng email
    isValid &=
        validator.kiemTraRong(newNv.email, 'tbEmail', "Vui lòng nhập email")
        && validator.kiemTraEmail(newNv.email, 'tbEmail', "Email không hợp lệ")
    // ! mật khẩu: kiểm tra rỗng, độ dài từ 6-10 kí tự chứa ít nhất 1 kí tự số, 1 kí tự in hoa, 1 kí tự đặc biệt
    isValid &=
        validator.kiemTraRong(newNv.matKhau, 'tbMatKhau', "Vui lòng nhập mật khẩu")
        && validator.kiemTraMatKhau(newNv.matKhau, 'tbMatKhau', "Mật khẩu chứa từ 6-10 kí tự, chứa ít nhất 1 kí tự số, 1 kí tự in hoa, 1 kí tự đặc biệt")
    // ! ngày tháng năm: kiểm tra rỗng và định dạng ngày
    isValid &=
        validator.kiemTraRong(newNv.ngayLam, 'tbNgay', "Vui lòng nhập ngày")
        && validator.kiemTraNgay(newNv.ngayLam, 'tbNgay', "Vui lòng nhập đúng định dạng tháng/ ngày/ năm")
    // ! lương: kiếm tra rỗng và   lương từ 1tr đến 20tr
    isValid &=
        validator.kiemTraRong(newNv.luongCoBan, 'tbLuongCB', "Vui lòng nhập lương")
        && validator.kiemTraMinMax(newNv.luongCoBan, 'tbLuongCB', "Lương từ 1 triệu đến 20 triệu", 1000000, 20000000);
    // ! Chức vụ:
    isValid &=
        validator.kiemTraChucVu(newNv.chucVu, 'tbChucVu', 'Vui lòng chọn chức vụ');
    // ! số giờ: rỗng và min max
    isValid &= validator.kiemTraRong(newNv.gioLamTrongThang, 'tbGiolam', 'Vui lòng nhập giờ làm')
        && validator.kiemTraMinMax(newNv.gioLamTrongThang, 'tbGiolam', 'Giờ làm từ 80-200 giờ', 80, 200);
    if (!isValid) {
        return;
    }

    // hiện trường tài khoản
    document.getElementById('tknv').disabled = false;
    document.getElementById('btnThemNV').disabled = false;
    // cập nhật object đó vào mảng
    //// ! tìm object cũ và thay object cũ = object mới
    for (var i = 0; i < nvArr.length; i++) {
        if (nvArr[i].taiKhoan == newNv.taiKhoan) {
            nvArr[i] = newNv; //* thay object cũ = object mới

        }
    }

    // cập nhật mảng lên json
    //// ! tạo json
    var nvArrJSON = JSON.stringify(nvArr);
    //// ! set lại giá trị
    localStorage.setItem(DANHSACHNHANVIEN, nvArrJSON);
    // render ra màn hình
    renderTable(nvArr);

    // ẩn model sau khi xử lý thành công:
    $('#myModal').modal('hide');
    $('body').removeClass('modal-open');
    $('body').css('padding-right', '0px');
    $('.modal-backdrop').remove();
    // xóa thông tin form
    resetForm();

}
function timNhanVien() {
    // tạo mảng chứa nhân viên cần tìm
    var arrSearch = [];
    // lấy value input
    var input = document.getElementById('searchName');
    var value = input.value;
    // so sánh với mảng.xếp loại
    for (var i = 0; i < nvArr.length; i++) {

        if (xuLyTiengViet(nvArr[i].xepLoai()).includes(xuLyTiengViet(value))) {
            arrSearch.push(nvArr[i]);
        }


    }
    // console.log('arrSearch: ', arrSearch);
    // render ra table
    renderTable(arrSearch);
}
function xuLyTiengViet(str) {
    // remove accents
    var from = "àáãảạăằắẳẵặâầấẩẫậèéẻẽẹêềếểễệđùúủũụưừứửữựòóỏõọôồốổỗộơờớởỡợìíỉĩịäëïîöüûñçýỳỹỵỷ",
        to = "aaaaaaaaaaaaaaaaaeeeeeeeeeeeduuuuuuuuuuuoooooooooooooooooiiiiiaeiiouuncyyyyy";
    for (var i = 0, l = from.length; i < l; i++) {
        str = str.replace(RegExp(from[i], "gi"), to[i]);
    }

    str = str.toLowerCase()
        .trim()
        .replace(/[^a-z0-9\-]/g, '-')
        .replace(/-+/g, '-');

    return str;
}





