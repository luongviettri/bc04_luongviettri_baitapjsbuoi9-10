// lưu các constructor
function NhanVien(taiKhoan, hoTen, email, matKhau, ngayLam, luongCoBan, chucVu, gioLamTrongThang) {
    this.taiKhoan = taiKhoan;
    this.hoTen = hoTen;
    this.email = email;
    this.matKhau = matKhau;
    this.ngayLam = ngayLam;
    this.luongCoBan = luongCoBan;
    this.chucVu = chucVu;
    this.gioLamTrongThang = gioLamTrongThang;
    this.tongLuong = () => {
        switch (this.chucVu) {
            case "Sếp": {
                return +this.luongCoBan * 3;
            }
            case "Trưởng phòng": {
                return +this.luongCoBan * 2;
            }
            case "Nhân viên": {
                return +this.luongCoBan;
            }
            default: {
                return "Không";
            }
        }
    }
    this.xepLoai = () => {
        var gioLam = this.gioLamTrongThang * 1;
        var gioLamXuatSac = 192;
        var gioLamGioi = 176;
        var gioLamKha = 160;
        if (gioLam >= gioLamXuatSac) {
            return "xuất sắc";
        } else if (gioLam >= gioLamGioi) {
            return "giỏi";
        } else if (gioLam >= gioLamKha) {
            return "khá";
        } else {
            return "trung bình"
        }
    }
}