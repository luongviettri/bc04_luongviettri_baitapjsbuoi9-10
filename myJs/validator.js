var validator = {
    kiemTraRong: (value, myDocument, message) => {
        if (value.length == 0) {
            document.getElementById(myDocument).innerHTML = message;
            return false;
        } else {
            document.getElementById(myDocument).innerHTML = '';
            return true;
        }
    },
    kiemTraDoDai: (value, myDocument, message, min, max) => {
        if (value.length < min || value.length > max) {
            document.getElementById(myDocument).innerHTML = message;
            return false;
        } else {
            document.getElementById(myDocument).innerHTML = '';
            return true;
        }
    },
    kiemTraKhongPhaiSo: (value, myDocument, message) => {
        var regex = /^([^0-9]*)$/;
        if (regex.test(value)) {
            document.getElementById(myDocument).innerHTML = '';
            return true;
        } else {
            document.getElementById(myDocument).innerHTML = message;
            return false;
        }
    },
    kiemTraEmail: (value, myDocument, message) => {
        const re =
            /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (re.test(value)) {
            document.getElementById(myDocument).innerHTML = '';
            return true;
        } else {
            document.getElementById(myDocument).innerHTML = message;
            return false;
        }
    },
    kiemTraNgay: (value, myDocument, message) => {
        var re = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
        if (re.test(value)) {
            document.getElementById(myDocument).innerHTML = '';
            return true;
        } else {
            document.getElementById(myDocument).innerHTML = message;
            return false;
        }
    },
    kiemTraMatKhau: (value, myDocument, message) => {
        var re = /^(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,10}$/;
        if (re.test(value)) {
            document.getElementById(myDocument).innerHTML = '';
            return true;
        } else {
            document.getElementById(myDocument).innerHTML = message;
            return false;
        }
    },
    kiemTraMinMax: (value, myDocument, message, min, max) => {
        if (value * 1 < min * 1 || value * 1 > max * 1) {
            document.getElementById(myDocument).innerHTML = message;
            return false;
        } else {
            document.getElementById(myDocument).innerHTML = '';
            return true;
        }
    },
    kiemTraChucVu: (value, myDocument, message) => {
        if (value == 'Chọn chức vụ' || value == '') {
            document.getElementById(myDocument).innerHTML = message;
            return false;
        } else {
            document.getElementById(myDocument).innerHTML = '';
            return true;
        }
    },
    kiemTraTrungTaiKhoan: (taiKhoan, myDocument, message, nvArr) => {
        if (nvArr.length > 0) {
            var biTrung = false;
            for (var i = 0; i < nvArr.length; i++) {
                item = nvArr[i];
                if (item.taiKhoan == taiKhoan) {
                    biTrung = true;
                }
            }
            if (biTrung) {
                document.getElementById(myDocument).innerHTML = message;
                return false;
            } else {
                document.getElementById(myDocument).innerHTML = '';
                return true;
            }
        }
        return true;
    }

}